# Translation of knetwalk into esperanto.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: knetwalk\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-10 00:43+0000\n"
"PO-Revision-Date: 2009-11-15 12:06+0100\n"
"Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: pology\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Axel Rousseau"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "axel@esperanto-jeunes.org"

#. i18n: ectx: property (toolTip), widget (QLabel, width)
#: customgame.ui:30
#, kde-format
msgid "Width of the grid"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, width)
#: customgame.ui:33
#, kde-format
msgid "Width:"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QCheckBox, kcfg_Wrapping)
#: customgame.ui:56
#, kde-format
msgid "Wrap from left to right and from top to bottom"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_Wrapping)
#: customgame.ui:59
#, kde-format
msgid "Wrapping"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QLabel, height)
#: customgame.ui:66
#, kde-format
msgid "Height of the grid"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, height)
#: customgame.ui:69
#, kde-format
msgid "Height:"
msgstr ""

#: gameview.cpp:167
#, kde-format
msgid ""
"Note: to win the game all terminals <strong>and all <em>cables</em></strong> "
"need to be connected to the server!"
msgstr ""

#: gameview.cpp:170
#, kde-format
msgid "The game is not won yet!"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QLabel, label)
#: general.ui:17
#, kde-format
msgid "Duration for cell to complete one rotation in milliseconds"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: general.ui:20
#, kde-format
msgid "Rotate duration:"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QCheckBox, kcfg_PlaySounds)
#: general.ui:40
#, kde-format
msgid "Play sounds on actions"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_PlaySounds)
#: general.ui:43
#, kde-format
msgid "Play sounds"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QCheckBox, kcfg_Autolock)
#: general.ui:60
#, kde-format
msgid "Automatically lock cell after rotation"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_Autolock)
#: general.ui:63
#, kde-format
msgid "Auto lock"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ReverseButtons)
#: general.ui:83
#, kde-format
msgid "Reverse mouse buttons"
msgstr ""

#. i18n: ectx: label, entry (username), group (Preferences)
#: knetwalk.kcfg:9
#, kde-format
msgid "Default user name"
msgstr ""

#. i18n: ectx: label, entry (PlaySounds), group (General)
#: knetwalk.kcfg:14
#, kde-format
msgid "Whether game sounds are played."
msgstr ""

#. i18n: ectx: label, entry (Autolock), group (General)
#: knetwalk.kcfg:18
#, kde-format
msgid "Whether lock cell after rotation."
msgstr ""

#. i18n: ectx: label, entry (ReverseButtons), group (General)
#: knetwalk.kcfg:22
#, kde-format
msgid "Use left click for clockwise and right click for counter-clockwise."
msgstr ""

#. i18n: ectx: label, entry (RotateDuration), group (General)
#: knetwalk.kcfg:26
#, kde-format
msgid "Duration for cell to complete one rotation."
msgstr ""

#. i18n: ectx: label, entry (Width), group (Custom)
#: knetwalk.kcfg:34
#, kde-format
msgid "The width of the board."
msgstr ""

#. i18n: ectx: label, entry (Height), group (Custom)
#: knetwalk.kcfg:40
#, kde-format
msgid "The height of the board."
msgstr ""

#. i18n: ectx: label, entry (Wrapping), group (Custom)
#: knetwalk.kcfg:46
#, kde-format
msgid "Whether wrap from left to right and from top to bottom is enabled."
msgstr ""

#. i18n: ectx: ToolBar (mainToolBar)
#: knetwalkui.rc:13
#, kde-format
msgid "Main Toolbar"
msgstr "Ĉefa ilobreto"

#: main.cpp:37
#, kde-format
msgid "KNetWalk"
msgstr ""

#: main.cpp:38
#, kde-format
msgid "KNetWalk, a game for system administrators."
msgstr ""

#: main.cpp:39
#, kde-format
msgid ""
"(C) 2004-2005 Andi Peredri, ported to KDE by Thomas Nagy\n"
"(C) 2007-2008 Fela Winkelmolen\n"
"(C) 2013 Ashwin Rajeev"
msgstr ""

#: main.cpp:44
#, kde-format
msgid "Andi Peredri"
msgstr "Andi Peredri"

#: main.cpp:45
#, kde-format
msgid "original author"
msgstr ""

#: main.cpp:48
#, kde-format
msgid "Thomas Nagy"
msgstr "Thomas Nagy"

#: main.cpp:49
#, kde-format
msgid "KDE port"
msgstr ""

#: main.cpp:52
#, kde-format
msgid "Ashwin Rajeev"
msgstr ""

#: main.cpp:53
#, kde-format
msgid "Port to QtQuick"
msgstr ""

#: main.cpp:56
#, kde-format
msgid "Eugene Trounev"
msgstr "Eugene Trounev"

#: main.cpp:57
#, kde-format
msgid "icon design"
msgstr ""

#: main.cpp:60
#, kde-format
msgid "Brian Croom"
msgstr ""

#: main.cpp:61
#, kde-format
msgid "Port to use the QGraphicsView framework"
msgstr ""

#: mainwindow.cpp:84
#, kde-format
msgid "Custom"
msgstr ""

#: mainwindow.cpp:130
#, kde-format
msgid "&Unlock All"
msgstr ""

#: mainwindow.cpp:135
#, kde-format
msgid "Keyboard: Field right"
msgstr ""

#: mainwindow.cpp:140
#, kde-format
msgid "Keyboard: Field left"
msgstr ""

#: mainwindow.cpp:145
#, kde-format
msgid "Keyboard: Field up"
msgstr ""

#: mainwindow.cpp:150
#, kde-format
msgid "Keyboard: Field down"
msgstr ""

#: mainwindow.cpp:155
#, kde-format
msgid "Keyboard: Turn clockwise"
msgstr ""

#: mainwindow.cpp:160
#, kde-format
msgid "Keyboard: Turn counterclockwise"
msgstr ""

#: mainwindow.cpp:165
#, kde-format
msgid "Keyboard: Toggle lock"
msgstr ""

#: mainwindow.cpp:176
#, kde-format
msgid "General"
msgstr ""

#: mainwindow.cpp:177
#, kde-format
msgid "Theme"
msgstr "Temo"

#: mainwindow.cpp:178
#, kde-format
msgid "Custom Game"
msgstr ""

#: mainwindow.cpp:187 mainwindow.cpp:244
#, kde-format
msgid "Moves Penalty"
msgstr ""

#: mainwindow.cpp:248
#, kde-format
msgid "Your score was %1, you did not make it to the high score list."
msgid_plural "Your score was %1, you did not make it to the high score list."
msgstr[0] ""
msgstr[1] ""

#: mainwindow.cpp:273
#, kde-format
msgctxt "Number of mouse clicks"
msgid "Moves: %1"
msgstr ""

#: mainwindow.cpp:274
#, kde-format
msgctxt "Time elapsed"
msgid "Time: %1"
msgstr ""

#: qml/main.qml:66
#, kde-format
msgid "Paused"
msgstr ""

#~ msgid "Fela Winkelmolen"
#~ msgstr "Fela Winkelmolen"

#~ msgid "New Game"
#~ msgstr "Nova ludo"

#~ msgid "The graphical theme to be used."
#~ msgstr "La uzota grafika etoso."

#~ msgid "The difficulty level."
#~ msgstr "La nivelo de malfacileco."

#~ msgid "Failed to load \"%1\" theme. Please check your installation."
#~ msgstr "Fiaskis ŝargi la \"%1\" etoson. Bonvolu kontroli vian instalaĵon."
